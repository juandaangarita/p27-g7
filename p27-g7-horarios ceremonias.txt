Planning Sprint
Descripción: Reunión para realizar la planeación del sprint 1 
Día de la semana: Viernes 17/09/2021
Hora: 1:30 pm
Duración: 20 minutos

Daily Scrum (de lunes a jueves)
Descripción: Revisión de avances por cada uno de los integrantes 
Día de la semana: Del 20-09-2021
Hora: 5:00 pm
Duración: 15 minutos

Revisión del Sprint (formador)
Descripción: Revisión del trabajo realizado en el sprint 1 por parte de Anyela
Día de la semana: viernes en la franja de 1:00 p.m. a 3:00 p.m.
Hora: Franja de 1:00pm a 3:00 pm
Duración: 10 Minutos

Retrospectiva del Sprint 
Descripción: 
Día de la semana:
Hora:
Duración:

Asesoría Tutor
Día de la semana: Por definir con el tutor
Hora: Por definir con el tutor
Duración: 10 minutos
